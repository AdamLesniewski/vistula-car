<footer class="footer">
        <div class="container">
            <div class="row">
                <nav class="bottom-nav col-md-6">
                    <ul class="nav nav-pills green-pills">
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Conditions of Use</a></li>
                        <li><a href="#">Press & Partners</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Help</a></li>
                    </ul>
                </nav>
                <nav class="bottom-nav col-md-3 col-md-offset-3">
                    <ul class="list-inline pull-right">
                        <li><a href="#"><span class="speech-bubble">Follow Us!</span></a></li>
                        <li><a href="#"><span class="socicon white">b</span></a></li>
                        <li><a href="#"><span class="socicon white">a</span></a></li>
                        <li><a href="#"><span class="socicon white">c</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <p class="small">© Service Searching Pty Ltd 2007-2015. All rights reserved.
                SERVICE SEARCHING® is a registered trade mark of Service Searching Pty
                Ltd.</p>
            </div>
        </div>
    </div>
</footer>